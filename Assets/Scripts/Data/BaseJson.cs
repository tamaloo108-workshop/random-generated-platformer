﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseJson
{
    public string id;
}

[System.Serializable]
public class LevelCombination : BaseJson
{
    public List<string> tiles;
}
