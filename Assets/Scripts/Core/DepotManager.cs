﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepotManager : MonoBehaviour
{
    public Dictionary<string, GameObject> DepotObjects = new Dictionary<string, GameObject>();
    static DepotManager instance;
    static string assetName;
    static string[] namesplit;

    public static Transform Self { get => instance.transform; }

    private void Start()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(this);
            instance = this;
        }
    }

    public static bool ValidateAssetAndRegister(GameObject newObject, params string[] objectAssetId)
    {
        assetName = newObject.name.ToString();
        for (int i = 0; i < objectAssetId.Length; i++)
        {
            namesplit = assetName.Split('_');

            if (namesplit[0].Contains(objectAssetId[i]))
            {
                AddToDepot(newObject, namesplit[0]);
                return true;
            }
        }

        return false;
    }

    public static void AddToDepot(GameObject newMember, string id)
    {
        if (instance.DepotObjects.ContainsKey(namesplit[0]))
        {
            instance.DepotObjects[id] = newMember;
        }
        else
        {
            instance.DepotObjects.Add(id, newMember);
            Debug.Log(id);
        }
    }

    public static void RefreshDepot()
    {
        instance.DepotObjects = new Dictionary<string, GameObject>();
    }


    public static GameObject SpawnToPool(string id, Transform parent = null)
    {
        return Instantiate(instance.DepotObjects[id], parent);
    }

    public static GameObject SpawnToPool(string id, Vector3 position)
    {
        return Instantiate(instance.DepotObjects[id], position, Quaternion.identity);
    }

}
