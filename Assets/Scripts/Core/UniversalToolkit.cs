﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class UniversalToolkit
{

    public static void LoadSceneAsync(string name, Action<AsyncOperation> callback, LoadSceneMode loadMode = LoadSceneMode.Additive)
    {
        SceneManager.LoadSceneAsync(name, loadMode).completed += callback;
    }

    public static void UnloadSceneAsync(string name, Action<AsyncOperation> callback)
    {
        SceneManager.UnloadSceneAsync(name).completed += callback;
    }

    public static void SetSceneActive(string gameSceneName)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(gameSceneName));
    }
}
