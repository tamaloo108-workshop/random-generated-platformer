using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Cinemachine.CinemachineVirtualCamera vCam;

    public void setTarget()
    {
        vCam.m_Follow = GameObject.FindGameObjectWithTag("Player").transform;
    }


}
