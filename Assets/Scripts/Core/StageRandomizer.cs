﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageRandomizer : MonoBehaviour
{
    [SerializeField]
    ScriptableObjectEvent.VoidEvent SetCameraTarget;

    public List<string> UsableTiles;
    [Range(10, 100)] public int TileCount = 20;

    protected List<TileData> Tiles = new List<TileData>();
    protected List<TileData> CombinedTiles = new List<TileData>();

    List<Transform> tempAvailableConnector = new List<Transform>();
    Transform tempConnectorParent;
    Transform tempConnector;
    Transform tempConnectorNext;
    Transform tempConnectorParentNext;

    GameObject tempTile;

    TileData tempTileDataNext;
    TileData tempTileData;

    bool OnSpawnNewTile => UnityEngine.Random.value > 0.4f && CombinedTiles.Count < TileCount;
    bool OnFirstInitTiles => CombinedTiles.Count == 0;

    public void StartRandomize()
    {
        RegisterTiles();
        RandomizeConnect();
        //spawn player should not be here
        GameObject g = DepotManager.SpawnToPool("ACT1", Vector3.zero);
        g.tag = "Player";
        g.transform.position = new Vector3(g.transform.position.x, 15, g.transform.position.z);
        SetCameraTarget.Invoke();
    }

    private void RegisterTiles()
    {
        //spawn tiles
        for (int i = 0; i < UsableTiles.Count; i++)
        {
            tempTile = DepotManager.SpawnToPool(UsableTiles[i]);

            tempAvailableConnector = new List<Transform>();
            //get Connectors
            tempConnectorParent = tempTile.transform.Find("Connector");
            for (int j = 0; j < tempConnectorParent.transform.childCount; j++)
            {
                tempAvailableConnector.Add(tempConnectorParent.transform.GetChild(j));
            }

            Tiles.Add(new TileData(UsableTiles[i], tempAvailableConnector, tempTile.transform));
        }
    }

    private void RandomizeConnect()
    {
        SpawnNewTile(TileCount);
        CombineAvailableTile();
    }

    private void CombineAvailableTile()
    {
        GameObject p = new GameObject("Stage");
        p.transform.position = Vector3.zero;

        for (int i = 1; i < CombinedTiles.Count - 1; i++)
        {
            tempTileData = CombinedTiles[i];
            tempConnectorParent = CombinedTiles[i].Parent;
            tempConnector = CombinedTiles[i].GetRandomAvailableConnector();

            tempTileDataNext = CombinedTiles[i + 1];
            tempConnectorParentNext = CombinedTiles[i + 1].Parent;
            tempConnectorNext = CombinedTiles[i + 1].GetRandomAvailableConnector();


            tempConnectorNext.SetParent(tempConnector);
            tempConnectorParentNext.SetParent(tempConnectorNext);

            tempConnectorNext.localPosition = Vector3.zero;
            tempConnectorNext.localRotation = Quaternion.AngleAxis(180f, Vector3.up);
            tempConnectorParentNext.SetParent(null);
            tempConnectorNext.SetParent(tempConnectorParentNext);

            tempConnector.gameObject.SetActive(false);
            tempConnectorNext.gameObject.SetActive(false);

            tempConnectorParent.SetParent(p.transform);
            tempConnectorParentNext.SetParent(p.transform);
        }
    }

    int selNum;
    private void SpawnNewTile(int count)
    {
        for (int i = 0; i < count; i++)
        {
            selNum = UnityEngine.Random.Range(0, Tiles.Count);
            tempTile = Instantiate(Tiles[selNum].Parent).gameObject;

            tempAvailableConnector = new List<Transform>();
            //get Connectors
            tempConnectorParent = tempTile.transform.Find("Connector");
            for (int j = 0; j < tempConnectorParent.transform.childCount; j++)
            {
                tempAvailableConnector.Add(tempConnectorParent.transform.GetChild(j));
            }

            tempTileData = new TileData(Tiles[selNum].id, tempAvailableConnector, tempTile.transform);
            CombinedTiles.Add(tempTileData);
        }
    }
}

[System.Serializable]
public class TileData
{
    public string id;
    public List<Transform> Connector;
    public Transform Parent;

    int selNum;

    public Transform GetRandomAvailableConnector()
    {
        selNum = UnityEngine.Random.Range(0, Connector.Count);
        if (Connector[selNum].gameObject.activeInHierarchy)
            return Connector[selNum];

        return GetRandomAvailableConnector();
    }

    public TileData(string id, List<Transform> connector, Transform parent)
    {
        this.id = id;
        Connector = connector;
        Parent = parent;
    }
}
