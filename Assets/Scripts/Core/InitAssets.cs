﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

public class InitAssets : MonoBehaviour
{
    public string[] AssetTypeToRegister;
    public string[] GameObjectIdToCheck;
    public string InitialSceneName;
    public string CoreSceneName;
    public string HudSceneName;
    public string GameSceneName;

    void Start()
    {
        //load default assets (built-in)
        Addressables.LoadResourceLocationsAsync("Default Load").Completed += LoadDefault_Completed;
        //remote
        Addressables.LoadResourceLocationsAsync("Basic Load").Completed += InitAssets_Completed;
    }

    private void OnLoadHud(AsyncOperation obj)
    {
        UniversalToolkit.LoadSceneAsync(GameSceneName, OnLoadGameScene);
    }

    private void OnLoadGameScene(AsyncOperation obj)
    {
        Debug.Log("done load scene");
        UniversalToolkit.SetSceneActive(GameSceneName);
        UniversalToolkit.LoadSceneAsync(HudSceneName, null);
        UniversalToolkit.UnloadSceneAsync(InitialSceneName, null);
    }

    private void LoadDefault_Completed(AsyncOperationHandle<IList<IResourceLocation>> obj)
    {
        for (int i = obj.Result.Count - 1; i >= 0; i--)
        {
            for (int j = 0; j < AssetTypeToRegister.Length; j++)
            {
                if (obj.Result[i].ResourceType.ToString() == AssetTypeToRegister[j])
                {
                    if (i == 0)
                        //load required assets (remote).
                        Addressables.LoadAssetAsync<GameObject>(obj.Result[i].PrimaryKey).Completed += LoadDefaultAssetLast;
                    else
                        Addressables.LoadAssetAsync<GameObject>(obj.Result[i].PrimaryKey).Completed += LoadDefaultAsset;
                }
            }
        }
    }

    private void InitAssets_Completed(AsyncOperationHandle<IList<IResourceLocation>> obj)
    {
        for (int i = obj.Result.Count - 1; i >= 0; i--)
        {
            for (int j = 0; j < AssetTypeToRegister.Length; j++)
            {
                if (obj.Result[i].ResourceType.ToString() == AssetTypeToRegister[j])
                {
                    if (i == 0)
                        Addressables.LoadAssetAsync<GameObject>(obj.Result[i].PrimaryKey).Completed += LoadRemoteAssetLast;
                    else
                        Addressables.LoadAssetAsync<GameObject>(obj.Result[i].PrimaryKey).Completed += LoadRemoteAsset;
                }
            }
        }
    }

    private void LoadDefaultAsset(AsyncOperationHandle<GameObject> obj)
    {
        DepotManager.ValidateAssetAndRegister(obj.Result, GameObjectIdToCheck);
    }

    private void LoadDefaultAssetLast(AsyncOperationHandle<GameObject> obj)
    {
        DepotManager.ValidateAssetAndRegister(obj.Result, GameObjectIdToCheck);

    }

    private void LoadRemoteAsset(AsyncOperationHandle<GameObject> obj)
    {
        DepotManager.ValidateAssetAndRegister(obj.Result, GameObjectIdToCheck);
    }

    private void LoadRemoteAssetLast(AsyncOperationHandle<GameObject> obj)
    {
        DepotManager.ValidateAssetAndRegister(obj.Result, GameObjectIdToCheck);

        UniversalToolkit.LoadSceneAsync(CoreSceneName, OnLoadHud);
    }
}
