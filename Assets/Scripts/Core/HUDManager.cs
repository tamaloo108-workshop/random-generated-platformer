﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDManager : MonoBehaviour
{
    [Header("Menu")]
    [SerializeField]
    ScriptableObjectEvent.VoidEvent StartGame;
    
    public void OnStartButtonClick()
    {
        StartGame.Invoke();
    }
}
