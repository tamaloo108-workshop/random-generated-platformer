﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim : MonoBehaviour
{
    Animator anim;

    public void PlayAnimation(ActorAnimStruct data)
    {
        anim = UniversalHelper.GetActorById(data.id).GetAnimator();
        anim.Play(data.animationName);
    }
}
