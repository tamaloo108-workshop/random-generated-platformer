﻿[System.Serializable]
public struct ActorAnimStruct
{
    public string id;
    public string animationName;

    public ActorAnimStruct(string id, string animationName)
    {
        this.id = id;
        this.animationName = animationName;
    }
}
