﻿namespace ScriptableObjectEvent
{

    [System.Serializable]
    public struct MovementStruct
    {
        public float dirX;
        public float dirY;
        public float speedAmount;
        public string actorReceiverId;

        public MovementStruct(float dirX, float dirY, float speedAmount)
        {
            this.dirX = dirX;
            this.dirY = dirY;
            this.speedAmount = speedAmount;
            actorReceiverId = "Non";
        }

        public MovementStruct(float dirX, float dirY, float speedAmount, string actorReceiverId)
        {
            this.dirX = dirX;
            this.dirY = dirY;
            this.speedAmount = speedAmount;
            this.actorReceiverId = actorReceiverId;
        }
    }
}