﻿namespace ScriptableObjectEvent
{
    [System.Serializable]
    public struct ActorStruct
    {
        public float speed;
        public bool isActive;
        public float xRange;
        public float yRange;

        public ActorStruct( float speed, bool isActive, float xRange, float yRange)
        {
            this.speed = speed;
            this.isActive = isActive;
            this.xRange = xRange;
            this.yRange = yRange;
        }
    }
}