﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjectEvent
{
    [System.Serializable]
    public struct Void { }
}