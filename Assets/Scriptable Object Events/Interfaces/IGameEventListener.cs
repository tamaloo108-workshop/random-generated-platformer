﻿namespace ScriptableObjectEvent
{
    public interface IGameEventListener<T>
    {
        void OnEventInvoke(T item);
    }
}