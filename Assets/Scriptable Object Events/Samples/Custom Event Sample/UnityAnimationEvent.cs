﻿using UnityEngine.Events;

namespace ScriptableObjectEvent
{
    [System.Serializable]
    public class UnityAnimationEvent : UnityEvent<ActorAnimStruct>
    {

    }
}