﻿using UnityEngine.Events;

namespace ScriptableObjectEvent
{
    [System.Serializable]
    public class UnityIntEvent : UnityEvent<int>
    {

    }
}