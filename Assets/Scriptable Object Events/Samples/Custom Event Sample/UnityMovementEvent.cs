﻿using UnityEngine.Events;

namespace ScriptableObjectEvent
{
    [System.Serializable]
    public class UnityMovementEvent : UnityEvent<MovementStruct>
    {

    }
}