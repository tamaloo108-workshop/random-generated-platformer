﻿using ScriptableObjectEvent;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimationInput
{
    protected string animName;
    protected PlayerActorDataHolder onAnim;
    protected ActorAnimStruct? _animData;

    public abstract void SetAnimInput();

    protected AnimationInput(string animName, PlayerActorDataHolder onAnim)
    {
        this.animName = animName;
        this.onAnim = onAnim;
    }
}


//base movement input.
public abstract class MovementInput
{
    protected float speed;
    protected BaseActorDataHolder onMove;
    protected MovementStruct? _movementData;

    public abstract bool isValidToMove { get; }
    public abstract void SetMoveInput();

    protected MovementInput(float speed, BaseActorDataHolder onMove)
    {
        this.speed = speed;
        this.onMove = onMove;
    }
}
