﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorProcessor : MonoBehaviour
{
    //NPC Delegate
    public delegate void onMoveCommand();
    public static event onMoveCommand OnNpcMoveCommand;

    //Player Delegate
    public delegate void onPlayerMoveCommand();
    public static event onPlayerMoveCommand OnPlayerMoveCommand;
    public delegate void onPlayMoveAnimCommand();
    public static event onPlayMoveAnimCommand OnPlayMoveAnimcommand;


    // Update is called once per frame
    void Update()
    {
        OnPlayerMoveCommand?.Invoke();
        OnNpcMoveCommand?.Invoke();
        OnPlayMoveAnimcommand?.Invoke();
    }
}
