﻿using System.Collections;
using UnityEngine;

public class PlayerActorDataHolder : BaseActorDataHolder
{
    [SerializeField] protected ScriptableObjectEvent.AnimationEvent onMoveAnimation;

    public ScriptableObjectEvent.AnimationEvent OnActorMoveAnim => onMoveAnimation;

    PlayerAnimationInput animInput;
    PlayerMovementInput input;
    private WaitForSeconds UpdateInputDelay;

    protected override void Start()
    {
        UpdateInputDelay = new WaitForSeconds(0.8f);
        input = new PlayerMovementInput(actorDataSO.actorData.speed, this);
        animInput = new PlayerAnimationInput("Run", this);

        StartCoroutine(UpdateInputValue());

        ActorProcessor.OnPlayerMoveCommand += input.SetMoveInput;
        ActorProcessor.OnPlayMoveAnimcommand += animInput.SetAnimInput;
    }

    private IEnumerator UpdateInputValue()
    {
        for (; ; )
        {
            input = new PlayerMovementInput(actorDataSO.actorData.speed, this);
            yield return UpdateInputDelay;
        }
    }

    private void OnDisable()
    {
        ActorProcessor.OnPlayerMoveCommand -= input.SetMoveInput;
        ActorProcessor.OnPlayMoveAnimcommand -= animInput.SetAnimInput;
    }

}
