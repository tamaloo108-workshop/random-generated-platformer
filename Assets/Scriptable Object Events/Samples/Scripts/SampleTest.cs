﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleTest : MonoBehaviour
{
    [SerializeField]
    ScriptableObjectEvent.VoidEvent OnGameStart;

    private void Start()
    {
        OnGameStart.Invoke();
    }


    public void WriteDebug()
    {
        Debug.Log("yes this will do, for now.");
    }

    public void CountTimeElapse(int times)
    {
        Debug.Log(times.ToString());
    }
}
