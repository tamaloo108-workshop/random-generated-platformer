﻿using UnityEngine;

namespace ScriptableObjectEvent
{
    [CreateAssetMenu(fileName = "NewActorData", menuName = "Scriptable Object Event/Data/Create Actor Data")]
    public class ActorData : ScriptableObject
    {
        public ActorStruct actorData;
    }

}