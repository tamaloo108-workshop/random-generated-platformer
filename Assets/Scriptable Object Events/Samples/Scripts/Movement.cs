﻿using ScriptableObjectEvent;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    protected Vector3 targetPosition;
    protected Vector3 calculatedTargetPosition;
    Vector3 moveDir;
    Vector3 camDir;
    float angle;
    public void MoveActor(MovementStruct movementData)
    {

        targetPosition = new Vector3(movementData.dirX, 0f, movementData.dirY);
        camDir = targetPosition.normalized;

        if (targetPosition.magnitude >= 0.1f)
            angle = Mathf.Atan2(camDir.x, camDir.z) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;

        moveDir = (Quaternion.Euler(0f, angle, 0f) * Vector3.forward);
        calculatedTargetPosition = Vector3.MoveTowards(UniversalHelper.GetActorById(movementData.actorReceiverId).position, UniversalHelper.GetActorById(movementData.actorReceiverId).position + moveDir, movementData.speedAmount * Time.deltaTime);
        UniversalHelper.GetActorById(movementData.actorReceiverId).rotation = Quaternion.Euler(0f, angle, 0f);
        UniversalHelper.GetActorById(movementData.actorReceiverId).position = calculatedTargetPosition;
        // UniversalHelper.GetActorById(movementData.actorReceiverId).rotation = Quaternion.RotateTowards(UniversalHelper.GetActorById(movementData.actorReceiverId).rotation, rot, 800f * Time.deltaTime);

    }

}

public static class Helpers
{
    private static Matrix4x4 _isoMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 45, 0));
    public static Vector3 ToIso(this Vector3 input) => _isoMatrix.MultiplyPoint3x4(input);
}
