﻿using UnityEngine;

namespace ScriptableObjectEvent
{

    [CreateAssetMenu(fileName ="NewVoidEvent", menuName ="Scriptable Object Event/Event/Create Void Event")]
    public class VoidEvent : BaseGameEvent<Void>
    {
        public void Invoke() => Invoke(new Void());

    }

}