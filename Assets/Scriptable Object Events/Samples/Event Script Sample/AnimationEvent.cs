﻿using UnityEngine;
namespace ScriptableObjectEvent
{
    [CreateAssetMenu(fileName = "NewVoidEvent", menuName = "Scriptable Object Event/Event/Create Animation Event")]
    public class AnimationEvent : BaseGameEvent<ActorAnimStruct>
    {
        public void Invoke() => Invoke(new ActorAnimStruct());

    }
}