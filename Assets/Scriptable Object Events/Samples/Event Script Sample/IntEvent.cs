﻿using UnityEngine;

namespace ScriptableObjectEvent
{
    [CreateAssetMenu(fileName = "NewIntEvent", menuName = "Scriptable Object Event/Event/Create Int Event")]
    public class IntEvent : BaseGameEvent<int>
    {

    }



}